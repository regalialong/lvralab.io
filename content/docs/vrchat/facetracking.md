---
weight: 900
title: Face & Eye Tracking
---

# Face & Eye Tracking

If you have a face and/or eye tracking solution and want to use it for social vr, one option is to use [OscAvMgr](https://github.com/galister/oscavmgr), which fills the same role as VRCFT does on Windows.